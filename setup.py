import os
from setuptools import setup

MAPY_VERSION = '0.1'

CLASSIFIERS = [
    "Development Status :: 1 - Beta",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Operating System :: POSIX :: Linux",
    "Programming Language :: Python :: 3.6",
    "Topic :: Scientific"
    ]

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname), encoding="utf-8").read()

setup(
    name='mapy',
    version=MAPY_VERSION,
    description='Plot Simple Maps using xarray, cartopy and matplotlib',
    author='Marcelo Rodrigues',
    author_email='marcelorodriguesss@gmail.com',
    url='www.unixlike.com.br',
    keywords='maps, plots, xarray, cartopy, matplotlib',
    license='GPLv3',
    classifiers=CLASSIFIERS
)
