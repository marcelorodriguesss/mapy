# Mapy

Command line tool for plot maps based on xarray, cartopy and matplotlib libraries.

## Requirements

* cmd2
* numpy
* matplotlib
* xarray
* cartopy

## Install

```
pip3 install -r requirements.txt --user
pip3 install . --user
```

## Examples
soon

## Contact

Marcelo Rodrigues <marcelorodriguesss@funceme.br>