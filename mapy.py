#!/usr/bin/env python3.6

import os
import cmd2
import atexit
import readline
import xarray as xr

import misc

history_file = os.path.expanduser('~/.mapy_history')
if not os.path.exists(history_file):
    with open(history_file, 'w') as fobj:
        fobj.write('')
readline.read_history_file(history_file)
atexit.register(readline.write_history_file, history_file)


class MyApp(cmd2.Cmd):

    prompt = 'yep? '

    intro = '\nWelcome to Mapy!\nTo help, type: help\n'

    def __init__(self):
        cmd2.Cmd.__init__(self)
        self.nc_filenames = {}
        self.nc_dsets = {}
        self.idx = 0

    def do_use(self, filename):
        """
        Read netcdf file.

        Examples
        --------
        >>> use sample.nc
        >>> use etopo.nc
        >>> use coads_climatology.cdf
        """
        with xr.open_dataset(filename, decode_times=True) as dset:
            self.nc_filenames[self.idx] = filename.args
            self.nc_dsets[self.idx] = dset
            self.idx += 1

    def do_sh(self, opt):
        """
        Show parameters.

        Parameters
        ----------
        d : str
            Show datasets

        Examples
        --------
        >>> use sample.nc
        >>> sh d
        """
        if opt.lower() == 'd' or opt.lower() == 'data':
            print('-' * 79)
            for key in self.nc_filenames:
                print(f'{key}) {self.nc_filenames[key]}')
                print(self.nc_dsets[key])
                print('-' * 79)

    def do_fill(self, params):
        """
        Plot variable from dataset using fill interpolation.

        Parameters
        ----------
        d : int
            Dataset ID
        var : str
            Variable name from dataset

        Examples
        --------
        >>> use sample.nc
        >>> sh d
        >>> # time, lon and lat depends of dataset, please
        >>> # check using 'sh d' command.
        >>> fill d=0/var=pcp/time=2019-02-01/lon=slice(-51.,-27.)/lat=slice(-22.,5.)
        """

        params = misc.get_params(params)
        dset = self.nc_dsets[params['d']]
        del params['d']
        misc.plotmap(dset, 'fill', **params)

    def do_shaded(self, params):
        """
        Plot variable from dataset using shaded grid point.

        Parameters
        ----------
        d : int
            Dataset ID
        var : str
            Variable name from dataset

        Examples
        --------
        >>> use sample.nc
        >>> sh d
        >>> # time, lon and lat depends of dataset, please
        >>> # check using 'sh d' command.
        >>> shaded d=0/var=pcp/time=2019-02-01/lon=slice(-51.,-27.)/lat=slice(-22.,5.)
        """

        params = misc.get_params(params)
        dset = self.nc_dsets[params['d']]
        del params['d']
        misc.plotmap(dset, 'shaded', **params)

    def do_EOF(self, line):
        return True


if __name__ == '__main__':
    app = MyApp()
    app.cmdloop()
