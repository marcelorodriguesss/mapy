import re
import cartopy as cart
from matplotlib import pyplot as plt


def get_params(params):

    params = (re.split(r'/', params))
    params = [p.split('=') for p in params]
    params = {k: v for k, v in params}

    if 'd' in params:
        if isinstance(params['d'], str):
            params['d'] = int(params['d'])

    # format numbers like: lons, lats and time
    char_rm = ['[', ']', 'slice', '(', ')']
    for k, v in params.items():
        if isinstance(v, str):
            if 'slice' in v:
                aux = params[k]
                for char in char_rm:
                    aux = aux.replace(char, '')
                i, j = aux.split(',')
                params[k] = slice(float(i), float(j))

    return params


def plotmap(dset, typeplot='fill', **params):

    var = params.pop('var', False)
    cpal = params.pop('cpal', 'jet_r')

    fig = plt.figure(figsize=(8, 8))

    proj = cart.crs.PlateCarree()

    ax = plt.axes(projection=proj)

    darray = dset[var].sel(params).squeeze(drop=True)

    if typeplot.lower() == 'shaded':
        darray.plot.pcolormesh(ax=ax, transform=proj, cmap=cpal, norm=None)
    elif typeplot.lower() == 'fill':
        darray.plot.contourf(ax=ax, transform=proj, cmap=cpal, norm=None)
    else:
        raise SyntaxError('Use shaded or fill to typeplot')

    states = cart.feature.NaturalEarthFeature(category='cultural',
                                              scale='50m', facecolor='none',
                                              name='admin_1_states_provinces_shp')

    ax.add_feature(states, edgecolor='k')

    countries = cart.feature.NaturalEarthFeature(category='cultural',
                                                 scale='50m', facecolor='none',
                                                 name='admin_0_countries')

    ax.add_feature(countries, edgecolor='k')

    ax.set_title(var)

    plt.show()

    plt.close()
